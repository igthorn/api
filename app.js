
/**
 * Igthorn (http://igthorn.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/igthorn for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/igthorn/blob/master/LICENSE.md MIT license
 */

// setting global constants

ROOT_PATH    = __dirname;
LIBRARY_PATH = ROOT_PATH + '/lib';

module.exports = (process.env['NODE_ENV'] === "COVERAGE")
  ? require('./lib-cov/express')
  : require('./lib/express');

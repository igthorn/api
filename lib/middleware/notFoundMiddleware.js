
/**
 * Igthorn (http://igthorn.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/igthorn for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/igthorn/blob/master/LICENSE.md MIT license
 */

"use strict";

var logger = require("../logger/logger");

module.exports = function (req, res) {
  logger.error("NotFoundMiddleware: Non matching routes found", {
    httpMethod: req.method,
    url: req.url,
    query: req.getParsedRequest().export()
  });
  res.status(404).json("Not Found");
};

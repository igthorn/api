
/**
 * Igthorn (http://igthorn.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/igthorn for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/igthorn/blob/master/LICENSE.md MIT license
 */

"use strict";

var requireDirectory = require("require-directory");
module.exports = requireDirectory(module, __dirname);


/**
 * Igthorn (http://igthorn.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/igthorn for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/igthorn/blob/master/LICENSE.md MIT license
 */

function Roles() {};

Roles.prototype.all = function (id, callback) {

};

Roles.prototype.get = function (id, callback) {

};

Roles.prototype.put = function (id, update, callback) {

};

Roles.prototype.post = function (name, data, callback) {

};

Roles.prototype.del = function (id, callback) {

};

module.exports = Roles;

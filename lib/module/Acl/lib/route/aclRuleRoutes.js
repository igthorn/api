
/**
 * Igthorn (http://igthorn.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/igthorn for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/igthorn/blob/master/LICENSE.md MIT license
 */

"use strict";

var appRootPath = require("app-root-path");
var logger      = require(appRootPath + "/lib/logger/logger");

exports.get = function (req, res) {

};

exports.getById = function (req, res) {

};

exports.create = function (req, res) {

};

exports.update = function (req, res) {

};

exports.replace = function (req, res) {

};

exports.delete = function (req, res) {

};

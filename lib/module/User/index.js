
/**
 * Igthorn (http://igthorn.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/igthorn for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/igthorn/blob/master/LICENSE.md MIT license
 */

/**
 * This file will load all User service's related files
 */

// export all routes
module.exports.Routes = {
  UserRoutes: require("./lib/route/userRoutes")
};

// export all services
module.exports.Services = {
  UsersService: require("./lib/service/usersService")
};

// export all gateways
module.exports.Gateways = {
  RethinkDbUserGateway: require("./lib/gateway/user/rethinkDbUserGateway")
};

// export all entities
module.exports.Entities = {
  UserEntity: require("./lib/entity/userEntity")
};

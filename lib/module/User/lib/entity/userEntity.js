
/**
 * Igthorn (http://igthorn.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/igthorn for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/igthorn/blob/master/LICENSE.md MIT license
 */

"use strict";

var appRootPath = require("app-root-path");
var uuid        = require("node-uuid");
var BaseEntity  = require(appRootPath + "/lib/entity/baseEntity");

class UserEntity extends BaseEntity
{
  constructor(data)
  {
    super();

    this.setFields({
      id: {type: "string", optional: false, def: uuid.v4()},
      username: {type: "string"},
      password: {type: "string"},
      email: {type: "string"},
      firstName: {type: "string"},
      middleName: {type: "string"},
      lastName: {type: "string"},
      avatar: {type: "string"},
      roleId: {type: "string"},
      status: {type: "string", optional: false, def: "active"},
      createdAt: {type: "date", optional: false, def: new Date()},
      updatedAt: {type: ["date", "null"], optional: false, def: null}
    });

    if (data) {
      this.inflate(data);
    }
  }

  getId()
  {
    return this.id;
  }

  setId(id)
  {
    this.id = id;
  }
}

module.exports = UserEntity;

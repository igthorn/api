
/**
 * Igthorn (http://igthorn.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/igthorn for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/igthorn/blob/master/LICENSE.md MIT license
 */

"use strict";

var appRootPath  = require("app-root-path");
var UsersService = require("../service/usersService");
var BaseService  = require(appRootPath + "/lib/service/baseService");
var BaseRoute    = require(appRootPath + "/lib/route/baseRoute");

class UserRoutes extends BaseRoute
{
  /**
   * Constructor allows to assign base boards service
   *
   * @param BaseService service Service
   */
  constructor(service)
  {
    if (!(service instanceof BaseService)) {
      service = new UsersService();
    }

    super(service);
  }
}

module.exports = UserRoutes;

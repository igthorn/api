
# Igthorn

[![Build Status](https://gitlab.com/igthorn/api/badges/v2.0.0/build.svg)](https://gitlab.com/igthorn/api/builds)

This is still early beta release as it's cut out from API of our product, lacking dev documentation and still not really standalone.

## Installation

You need to have node.js v6+ installed. The easiest way to start is to spin docker instance of RethinkDB as following:

```
sudo docker run -itP --name rethinkdb-stable --detach rethinkdb
sudo docker inspect rethinkdb-stable
```

At the end of JSON output there's "IPAddress" which needs to be copied to config/app.json into rethinkdb->host node.

Now you can build database(from main project directory):

```
npm install
node build/RethinkDb/init.js
```

As config of database and database itself is setup - last thing to do is to start Igthorn server:

```
node app.js
```


## Usage

Igthorn is an RESTful API with multiple resources. You can query the API with usage of prepared URL.

List of available resources:
- api-accounts
- boards
- comments
- lanes
- lane-tag-links
- projects
- sprints
- tags
- tickets
- ticket-activities
- ticket-links
- ticket-statuses
- time-logs
- users

Sample queries:

Get all comments:

GET: http://your-url.com/tickets/:ticketId/comments

Get all boards for project 1:

GET: http://your-url.com/projects/:projectId/boards?conditions[projectId]=1

Get all projects created after 1st of Jan 2014:

GET: http://your-url.com/projects?conditions[createdAt][gt]=01.01.2014

Get all tickets ordered descending by created date:

GET: http://your-url.com/tickets?order[createdAt]=desc

Get last 3 sprints ordered descending by created date:

GET: http://your-url.com/tickets?order[createdAt]=desc&limit=3

Get ticket's comments with joined with user

GET: http://your-url.com/ticket/:ticketId/comments/:commentId?fields[]=t.id&fields[]=t.name&fields[]=t.status&fields[]=t.userId&fields[u.username]=user&joins[]=user

The table below lists the standard methods that have a well-defined meaning for all resources and collections.


|  Method |   Scope    |     reThinkDb            |              Semantics                                    |
|:-------:|:----------:|:------------------------:|:---------------------------------------------------------:|
|   GET   | collection |        r.table()         | Retrieve all resources in a collection                    |
|   GET   |  resource  |     r.table().get()      | Retrieve a single resource                                |
|   HEAD  | collection |           -              | Retrieve all resources in a collection (header only)      |
|   HEAD  |  resource  |           -              | Retrieve a single resource (header only)                  |
|   POST  | collection |    r.table().insert()    | Create a new resource in a collection (drop id if passed) |
|   PUT   |  resource  |    r.table().get() &&    | Replace resource                                          |
|         |            |     record.replace()     |                                                           |
|  PATCH  |  resource  | r.table().get().update() | Update a resource                                         |
|  DELETE |  resource  |     r.get().delete()     | Delete a resource                                         |
| OPTIONS |     any    |           -              | Return available HTTP methods and other options           |

For more example please refer to test/functional where you can find single test suite file
per resource.

## Todo

* Rework Route, Service and Molk modules
* Implement HAWK authentication

## License

[MIT](license.md)

## Contributing

We would love to see your contributions to this repository, so if you found a bug, created some fix/functionality or just have an idea, please feel free to create pull request or issue in GitLab.

Thanks in advance,

HedonSoftware team

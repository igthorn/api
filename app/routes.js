
/**
 * Igthorn (http://igthorn.com/)
 *
 * @link      https://gitlab.com/HedonSoftware/igthorn for the canonical source repository
 * @copyright Copyright (c) 2014-2016 HedonSoftware Limited (http://www.hedonsoftware.com)
 * @license   https://gitlab.com/HedonSoftware/igthorn/blob/master/LICENSE.md MIT license
 */

"use strict";

var appRootPath = require("app-root-path");
var middleware  = require(appRootPath + "/lib/middleware");

var UserRoutes = require(appRootPath + "/lib/module/User").Routes.UserRoutes;
var userRoutes = new UserRoutes();

//var AclRoutes = require(appRootPath + "/lib/module/Acl").Routes.AclRoutes;
//var aclRoutes = new AclRoutes();

//var ApiAccountRoutes = require(appRootPath + "/lib/module/ApiAccount").Routes.ApiAccountRoutes;
//var apiAccountRoutes = new ApiAccountRoutes();

module.exports = function(router) {

  // user resources
  router.param("id", middleware.validatorMiddleware.id);
  router.get("/users", userRoutes.get.bind(userRoutes));
  router.get("/users/:id", userRoutes.getById.bind(userRoutes));
  router.post("/users", userRoutes.create.bind(userRoutes));
  router.patch("/users/:id", userRoutes.update.bind(userRoutes));
  // router.put("/users/:id", userRoutes.replace.bind(userRoutes));
  router.delete("/users/:id", userRoutes.delete.bind(userRoutes));

  // // acl-roles resources
  // router.param("id", middleware.validatorMiddleware.id);
  // router.get("/acl-roles", aclRoleRoutes.get.bind(aclRoleRoutes));
  // router.get("/acl-roles/:id", aclRoleRoutes.getById.bind(aclRoleRoutes));
  // router.post("/acl-roles", aclRoleRoutes.create.bind(aclRoleRoutes));
  // router.patch("/acl-roles/:id", aclRoleRoutes.update.bind(aclRoleRoutes));
  // // router.put("/acl-roles/:id", aclRoleRoutes.replace.bind(aclRoleRoutes));
  // router.delete("/acl-roles/:id", aclRoleRoutes.delete.bind(aclRoleRoutes));

  // // acl-resources resources
  // router.param("id", middleware.validatorMiddleware.id);
  // router.get("/acl-resources", aclResourceRoutes.get.bind(aclResourceRoutes));
  // router.get("/acl-resources/:id", aclResourceRoutes.getById.bind(aclResourceRoutes));
  // router.post("/acl-resources", aclResourceRoutes.create.bind(aclResourceRoutes));
  // router.patch("/acl-resources/:id", aclResourceRoutes.update.bind(aclResourceRoutes));
  // // router.put("/acl-resources/:id", aclResourceRoutes.replace.bind(aclResourceRoutes));
  // router.delete("/acl-resources/:id", aclResourceRoutes.delete.bind(aclResourceRoutes));

  // // acl-rule resources
  // router.param("id", middleware.validatorMiddleware.id);
  // router.get("/acl-rules", aclRuleRoutes.get.bind(aclRuleRoutes));
  // router.get("/acl-rules/:id", aclRuleRoutes.getById.bind(aclRuleRoutes));
  // router.post("/acl-rules", aclRuleRoutes.create.bind(aclRuleRoutes));
  // router.patch("/acl-rules/:id", aclRuleRoutes.update.bind(aclRuleRoutes));
  // // router.put("/acl-rules/:id", aclRuleRoutes.replace.bind(aclRuleRoutes));
  // router.delete("/acl-rules/:id", aclRuleRoutes.delete.bind(aclRuleRoutes));
};
